GF312H - Garmin fenix 3 - 12 hour watchface
===============================================

Description
-----------
This watch face shows the time on a 12 hour scale. With date and battery percentage indicators.
Version history
---------------
###V1.1:
+ support for D2 Bravo
+ minor fixes (style etc.)
###V1.0:
+ 12 hour clock marks
+ 60 minute clock marks
+ shows day, day of the week and month (as number)
+ battery indicator

Garmin forum
------------
