using Toybox.System as Sys;

module GF3Helper {

    function getHourUI(hour, is24Hour) {
        if (!is24Hour) {
            if(hour == 0) {
                return [12, "AM"];
            } else if(hour == 12) {
                return [12, "PM"];
            } else if(hour < 12){
                return [hour.toNumber(), "AM"];
            } else {
                return [(hour.toNumber() % 12), "PM"];
            }
        }
        return [hour.toNumber(), ""];
    }
}