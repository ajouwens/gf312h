using Toybox.Application as App;

class GF312HWatch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF312H()];
    }
}
